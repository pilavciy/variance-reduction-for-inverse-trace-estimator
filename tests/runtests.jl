using RandomForests,LightGraphs,SimpleWeightedGraphs
using Test
using LinearAlgebra
using Distributions
using Random
include("../utils/utils.jl")

#test that the result of random_forest is correct
#ie. it should be a spanning forest, oriented towards the roots
function check_correctness(rf)
    F = LightGraphs.SimpleDiGraph(rf)
    roots =collect(rf.roots)
    @test !LightGraphs.is_cyclic(F)
    @test all(LightGraphs.outdegree(F,roots) .== 0)
    #test that all nodes lead to a root
    for cc in LightGraphs.connected_components(F)
        if length(cc)==1
            @test cc[1] in roots
        else
            rc = intersect(cc,roots)
            #there should be a single root per connected component
            @test length(rc)==1
            rc = rc[1]
            bf=LightGraphs.bfs_parents(F,rc;dir=-1)[cc]
            @test all(bf .> 0)
        end
    end
end

## Check CV implementations
function check_boundary_track(G,rf,bt)
    for i in 1 : LightGraphs.nv(G)
        @test bt[i] == sum(rf.root[LightGraphs.neighbors(G,i)] .!= rf.root[i])
    end
end
function check_root_boundary_track(G,rf,bt)
    neigh_sum = 0
    for i in collect(rf.roots)
        neigh_sum += sum(rf.root[LightGraphs.neighbors(G,i)] .!= i)
    end
    @test bt == neigh_sum
end

## Check stratified sampling implmentations
function check_ss_forests(rf,fl_roots)
    @test intersect(fl_roots , rf.roots) == fl_roots
end

const testdir = dirname(@__FILE__)
tests = [
    "forest","probfuncs","cv"
]

@testset "tr_var_red" begin
    for t in tests
        tp = joinpath(testdir, "$(t).jl")
        include(tp)
    end
end
