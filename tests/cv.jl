@testset "cv" begin
    Random.seed!(1)
    n = 1000
    gg = LightGraphs.barabasi_albert(n,10)
    L = LightGraphs.laplacian_matrix(gg)
    for i in 1:100
        rf,bt = random_forest_boundary_track(gg,i)
        p = Partition(rf)
        Sbar = p*Matrix(I(n))
        part=Int.(denserank(rf.root))
        sizep = counts(part)
        diag_S = 1 ./ sizep[part]
        @test tr(L*Sbar) ≈ (bt'*diag_S)

        rf,cont_var = random_forest_root_boundary_track(gg,i)
        Stilde = rf*Matrix(I(n))
        @test tr(L*Stilde) ≈ cont_var
    end
end
