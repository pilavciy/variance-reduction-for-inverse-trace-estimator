using LightGraphs
using PyPlot
using LinearAlgebra
using JLD2
using SparseArrays
using StatsBase
using LaTeXStrings
using StatsBase
using GraphIO
using Random

pygui(false)
close("all")
## Define a graph
graphs = ["K-regular","Barabasi-Albert","Collab-CM","Citation-HEP","3D-Grid","Amazon"]
row_num = 2
col_num = Int64(ceil(length(graphs)/row_num))
fs = 20
f = figure(figsize=[10,10])
for r = 1 : row_num
    a= subplot(row_num,col_num,(r-1)*col_num+1)

    ylabel("Effective Runtime (sec.)",fontsize=fs)
    for c = 1 : col_num
        gname=graphs[(r-1)*col_num+c]
        outputfolder = string("./outputs/",gname)
        JLD2.@load string(outputfolder,"/workspace.jld2") G sq_over_n exact Q NREP EXPREP eff_time_direct eff_time_amg eff_time_pcg eff_time_cg eff_time_rsf eff_time_rsf_cv eff_time_rsf_cv2 eff_time_rsf_st eff_time_rsf_st_appr var_direct var_amg var_pcg var_cg var_rsf var_rsf_cv var_rsf_cv2 var_rsf_st var_rsf_st_appr
        display("gname:$gname,nv:$(LightGraphs.nv(G)),ne:$(LightGraphs.ne(G))")

        tol = 0.002

        std_direct = sqrt.(mean(var_direct[:,2:end],dims=2))
        k_direct = ((std_direct ./ ( tol .* (exact) )).^2)

        std_amg = sqrt.(mean(var_amg[:,2:end],dims=2))
        k_amg = ((std_amg ./ ( tol .* (exact) )).^2)

        std_pcg = sqrt.(mean(var_pcg[:,2:end],dims=2))
        k_pcg = ((std_pcg ./ ( tol .* (exact) )).^2)

        std_cg = sqrt.(mean(var_cg[:,2:end],dims=2))
        k_cg = ((std_cg ./ ( tol .* (exact) )).^2)

        std_rsf = sqrt.(mean(var_rsf[:,2:end],dims=2))
        k_rsf = ((std_rsf ./ ( tol .* (exact) )).^2)

        std_rsf_cv = sqrt.(mean(var_rsf_cv[:,2:end],dims=2))
        k_rsf_cv = ((std_rsf_cv ./ ( tol .* (exact) )).^2)

        std_rsf_cv2 = sqrt.(mean(var_rsf_cv2[:,2:end],dims=2))
        k_rsf_cv2 = ((std_rsf_cv2 ./ ( tol .* (exact) )).^2)

        std_rsf_st = sqrt.(mean(var_rsf_st[:,2:end],dims=2))
        k_rsf_st = ((std_rsf_st ./ ( tol .* (exact) )).^2)

        std_rsf_st_appr = sqrt.(mean(var_rsf_st_appr[:,2:end],dims=2))
        k_rsf_st_appr = ((std_rsf_st_appr ./ ( tol .* (exact) )).^2)

        ## Time-ϵ Plot
        n = LightGraphs.nv(G)
        R_generation_time = 0
        for e = 1 : 2
            R_generation_time = @elapsed Float64.(2 .* bitrand(n,NREP) .- 1)
        end

        a= subplot(row_num,col_num,(r-1)*col_num+c)
        a.title.set_text(gname )
        a.title.set_fontsize(25)
        eff_time_direct = mean(eff_time_direct[:,2:end],dims=2)
        plot(sq_over_n,(eff_time_direct .+ R_generation_time) .* k_direct,marker="x",linewidth=3.0,markersize=10)
        # text(sq_over_n[end]+0.01, eff_time_direct[end] * k_direct[end],"direct")

        eff_time_amg = mean(eff_time_amg[:,2:end],dims=2)
        plot(sq_over_n,(eff_time_amg .+ R_generation_time) .* k_amg,marker="d",linewidth=3.0,markersize=10)
        # text(sq_over_n[end]+0.01, eff_time_amg[end] * k_amg[end],"amg")

        eff_time_pcg = mean(eff_time_pcg[:,2:end],dims=2)
        plot(sq_over_n,(eff_time_pcg) .* k_pcg,marker="H",linewidth=3.0,markersize=10)

        # text(sq_over_n[end]+0.01, eff_time_pcg[end] * k_pcg[end],"pcg")
        eff_time_cg = mean(eff_time_cg[:,2:end],dims=2)
        plot(sq_over_n,(eff_time_cg.+ R_generation_time) .* k_cg,marker="o",linewidth=3.0,markersize=10)
        # text(sq_over_n[end]+0.01, eff_time_cg[end] * k_cg[end],"cg")

        eff_time_rsf = mean(eff_time_rsf[:,2:end],dims=2)
        plot(sq_over_n,(eff_time_rsf) .* k_rsf,marker="h",linewidth=3.0,markersize=10)
        # text(sq_over_n[end]+0.01, eff_time_rsf[end] * k_rsf[end],"rsf")

        eff_time_rsf_cv = mean(eff_time_rsf_cv[:,2:end],dims=2)
        plot(sq_over_n,(eff_time_rsf_cv) .* k_rsf_cv,marker="*",linewidth=3.0,markersize=10)
        # text(sq_over_n[end]+0.01, eff_time_rsf_cv[end] * k_rsf_cv[end],"rsf-cv")

        eff_time_rsf_cv2 = mean(eff_time_rsf_cv2[:,2:end],dims=2)
        plot(sq_over_n,(eff_time_rsf_cv2 ) .* k_rsf_cv2,marker="D",linewidth=3.0,markersize=10)
        # text(sq_over_n[end]+0.01, eff_time_rsf_cv2[end] * k_rsf_cv2[end],"rsf-cv2")

        eff_time_rsf_st_appr = mean(eff_time_rsf_st_appr[:,2:end],dims=2)
        plot(sq_over_n, (eff_time_rsf_st_appr) .* k_rsf_st_appr,marker="s",linewidth=3.0,markersize=10)
        # text(sq_over_n[end]+0.01, mean(eff_time_rsf_st_appr,dims=2)[end] * k_rsf_st_appr[end],"rsf-st-appr")
        xscale("log")
        yscale("log")

        xlabel("tr(K)/n",fontsize=20)
        PyPlot.xticks(fontsize=13)
        tick_params(axis="x",which="minor")
        PyPlot.yticks(fontsize=13)
        PyPlot.grid(true)
    end
end
figlegend(["direct","amg","pcg","cg","rsf",latexstring("\\bar{s}"),latexstring("\\tilde{s}"),latexstring("{s_{st}}")],loc="upper center",ncol = 4,fontsize=20, bbox_to_anchor=(0.5, 0.0),framealpha=1.0)
f.subplots_adjust(bottom=0.15,left=0.1,hspace=0.30)
# f.tight_layout(rect=[0,0,1.0,1.0])
savefig("../Variance Reduction for Inverse Trace Estimation/figs/gretsi_2rows_fig.pdf",bbox_inches="tight")
