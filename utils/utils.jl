function random_forest_boundary_track(G::LightGraphs.AbstractGraph,q::Real)
    n = LightGraphs.nv(G)
    in_tree = falses(n)
    roots = BitSet()
    root = zeros(Int64,n)
    next = zeros(Int64,n)
    nroots = 0
    d = LightGraphs.degree(G)
    boundary_track = zeros(Int64,n)
    @inbounds for i in 1:n
        u = Int64(i)
        while ( !in_tree[u] )
            rn = rand()
            if ( rn < (q / (q+d[u])) )
                in_tree[u] = true
                push!(roots,u)
                nroots += 1
                root[u] = u
                next[u] = 0

                neighs = LightGraphs.neighbors(G, u)
                root[u] = u
                diff_root_neighs = (root[neighs] .!= u) .& (in_tree[neighs])
                boundary_track[u] = sum(diff_root_neighs)
                boundary_track[neighs] .+= diff_root_neighs
            else
                neighs = LightGraphs.neighbors(G, u)
                next[u] = rand(neighs)
                u = next[u]
            end
        end

        r = root[u]
        #Retrace steps, erasing loops
        u = i
        while (!in_tree[u])
            neighs = LightGraphs.neighbors(G, u)
            root[u] = r
            diff_root_neighs = (root[neighs] .!= r) .& (in_tree[neighs])
            boundary_track[u] = sum(diff_root_neighs)
            boundary_track[neighs] .+= diff_root_neighs
            in_tree[u] = true
            u = next[u]
        end
    end
    RandomForest(next,roots,nroots,root),boundary_track
end

function random_forest_root_boundary_track(G::LightGraphs.AbstractGraph,q::Real)
    n = LightGraphs.nv(G)
    in_tree = falses(n)
    roots = BitSet()
    root = zeros(Int64,n)
    next = zeros(Int64,n)
    nroots = 0
    d = LightGraphs.degree(G)
    boundary_track = 0
    root_neighs = falses(n)
    @inbounds for i in 1:n
        u = Int64(i)
        while ( !in_tree[u] )
            rn = rand()
            if ( rn < (q / (q+d[u])) )
                in_tree[u] = true
                nroots += 1
                root[u] = u
                next[u] = 0
                push!(roots,u)

                neighs = LightGraphs.neighbors(G, u)
                neighs_intree = neighs[in_tree[neighs] .== true]
                neighs_outtree = neighs[in_tree[neighs] .== false]
                root_neighs[neighs_outtree] .= true
                boundary_track += (sum(root[neighs_intree] .!= u) + sum(root[neighs_intree] .== neighs_intree) )
            else
                neighs = LightGraphs.neighbors(G, u)
                next[u] = rand(neighs)
                u = next[u]
            end
        end

        r = root[u]
        #Retrace steps, erasing loops
        u = i
        while (!in_tree[u])
            if(root_neighs[u])
                neighs = LightGraphs.neighbors(G, u)
                boundary_track += length(intersect(setdiff(roots,r),neighs))
            end
            root[u] = r
            in_tree[u] = true
            u = next[u]
        end
    end
    RandomForest(next,roots,nroots,root),boundary_track
end


function my_quantile(pmf::Array,p::Array)
    c = pmf[1]
    p_counter = 1
    strat_f = []
    strat_p = []
    i = 0;
    while (p_counter != length(p)+1)
        if(c > p[p_counter])
            push!(strat_f,i)
            p_counter+=1
            if(isempty(strat_p))
                push!(strat_p,c)
            else
                push!(strat_p,c - sum(strat_p))
            end
        end
        i += 1
        c += pmf[i+1]
    end
    push!(strat_p,1-sum(strat_p))
    strat_f,strat_p
end
function random_forest_cond_first_layer(G::LightGraphs.AbstractGraph,q::Real,roots::BitSet;plot_flag=false)

    n = LightGraphs.nv(G)
    in_tree = falses(n)
    in_tree[collect(roots)] .= true
    root = zeros(Int64,n)
    root[collect(roots)] .= collect(roots)
    nroots = length(roots)
    d = LightGraphs.degree(G)
    skip_node = trues(n)
    next = zeros(Int64,n)
    @inbounds for i in 1:n
        u = Int64(i)
        while (!in_tree[u] )
            if(skip_node[u])
                skip_node[u] = false
                nbrs = LightGraphs.neighbors(G, u)
                next[u] = rand(nbrs)
                u = next[u]
            else
                if ( (q+d[u])*rand() < q  )
                    in_tree[u] = true
                    push!(roots,u)
                    nroots += 1
                    root[u] = u
                    next[u] = 0
                else
                    nbrs = LightGraphs.neighbors(G, u)
                    next[u] = rand(nbrs)
                    u = next[u]
                end
            end
        end
        r = root[u]
        #Retrace steps, erasing loops
        u = i
        while (!in_tree[u])
            root[u] = r
            in_tree[u] = true
            u = next[u]
        end
    end
    RandomForest(next,roots,nroots,root)
end


function separator2bins(strat_f,n)
    strat_bins = zeros(length(strat_f)+1,2)
    strat_bins[1,1] = -1
    strat_bins[1,2] = strat_f[1]
    for i = 2 : length(strat_f)
        strat_bins[i,1] = strat_f[i-1]
        strat_bins[i,2] = strat_f[i]
    end
    strat_bins[end,1] = strat_f[end]+1
    strat_bins[end,2] = n
    strat_bins
end

function calc_strat_prob(d::Distribution,strat_f::Array{Int64},n::Int64)
    strat_p = zeros(length(strat_f) + 1)
    strat_p[1] = cdf(d,strat_f[1])
    for i = 2 : length(strat_f)
        k_low =strat_f[i-1]
        k_high =strat_f[i]

        p1 = cdf(d,k_low)
        p2 = cdf(d,k_high)
        strat_p[i] = p2 - p1
    end
    strat_p[end] = 1 - cdf(d,strat_f[end])
    strat_p
end

function noisy_kband_signal_generator(L,k::Int64,SNR::Float64,signorm::Float64)
    N = size(L,1)
    v, Uk, info = KrylovKit.eigsolve(L + eps()*I, k, :SR, krylovdim = max(KrylovDefaults.krylovdim, 2*k))
    Uk = reduce(hcat, Uk[1:k])
    ## create a signal to filter
    x_fourier = rand(k)
    x = Uk*x_fourier
    x = signorm*x ./ norm(x)
    σ =  signorm / sqrt(N * SNR)
    x,x + randn(N)*σ
end

function cond_bernouilli_sampler_app(w::Array,k::Int64;maxiter=100)
    ## This function implements the MC approach in this paper:
    # https://arxiv.org/pdf/2012.03103.pdf

    if(k == 0)
        return []
    end
    n = length(w)
    S_1 = Distributions.sample(1:n,k,replace=false)
    S_0 = mysetdiff(n,BitSet(S_1))
    for i = 1 : maxiter
        i_0_idx = rand(1:n-k)
        i_1_idx = rand(1:k)
        i_0 = S_0[i_0_idx]
        i_1 = S_1[i_1_idx]
        if(rand() < min(1,w[i_0] / w[i_1]))
            temp = i_0
            S_0[i_0_idx] = i_1
            S_1[i_1_idx] = temp
        end
    end
    S_1
end

function cond_bernouilli_sampler(prob_vec::Array,Srange::Array)
    S = -1
    l = length(prob_vec)
    ber_vec = similar(prob_vec)
    while !(S > Srange[1] && S <= Srange[2])
        ber_vec = (rand(l) .< prob_vec)
        S = sum(ber_vec)
    end
    return findall(ber_vec .== 1)
end
