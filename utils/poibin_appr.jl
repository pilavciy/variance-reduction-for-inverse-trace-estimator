function saddle_point_quantile(prob_vec,α)
    ## NOT WORKING AT THE MOMENT!!!!!
    n = length(prob_vec)
    sqrtn = sqrt(n)
    N = Normal(0,1)
    K_n(t) = 1/n*(sum(log.(1 .- prob_vec .+ prob_vec*exp(t*sqrtn) )))
    K_n_prime(t) = 1/sqrtn*(sum( (prob_vec .* exp(t*sqrtn) ) ./ (1 .- prob_vec .+ prob_vec .* exp(t*sqrtn)) )  )
    K_n_primex2(t) = (sum( (prob_vec .* exp(t*sqrtn) .* (1 .- prob_vec) ) ./ ((1 .- prob_vec .+ prob_vec .* exp(t*sqrtn)).^2) )  )
    z_n(t) = t*sqrt(n*K_n_primex2(t))
    ξ_n(t) = sqrt(2*n*(t*K_n_prime(t) - K_n(t)))*sign(t)
    r_n(t) = ξ_n(t) + (1/ξ_n(t)) * log(z_n(t)/ξ_n(t))
    z_α = quantile(N,1-α)

    τ = z_α + (z_α - sqrtn*K_n_prime(z_α/sqrtn)) / K_n_primex2(z_α/sqrtn)
    z_α_star = sqrtn *K_n_prime(τ/sqrtn)
    s = z_α_star + (ξ_n(τ/sqrtn) * (z_α - r_n(τ/sqrtn)) )/ τ
    return s
end


function Cornish_Fisher_quantile(prob_vec::Array{Float64},α::Array{Float64})
    ## WORKS FINE
    μ = sum(prob_vec)
    σ2 = sum( prob_vec .* (1 .- prob_vec))
    skw = σ2^(-3/2) * sum( (1 .- 2 .* prob_vec) .* ( 1 .- prob_vec) .* prob_vec)
    kurt = σ2^(-2) * sum( (1 .- 6 .* (1 .- prob_vec) .* prob_vec) .* ( 1 .- prob_vec) .* prob_vec)

    b = Normal()
    z_alphas = quantile.(b,α)
    w = x -> (x + (skw/6)*((x^2) - 1) + (kurt/24)*x*(x^2 - 3) - ((skw^2)/36)*x*(2*(x^2) - 5))
    qtl =μ .+ σ2^(1/2) .* w.(z_alphas)
end
