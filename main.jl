using RandomForests
using LightGraphs
using PyPlot
using LinearAlgebra
using JLD2
using SparseArrays
using StatsBase
using LaTeXStrings
using Distributions
using SpecialFunctions
using StatsBase
using BenchmarkTools
using KrylovKit
using Optim
using AlgebraicMultigrid
using IterativeSolvers
using KrylovMethods
using LoopVectorization
using ProgressBars
using Random
using GraphIO
using SNAPDatasets
using Graphs
close("all")
pygui(true)
include("./utils/utils.jl")
include("./utils/ssl_data_loaders.jl")
LinearAlgebra.BLAS.set_num_threads(1)
## Define a graph
graphs = ["US-Patent","Google"]
for gname in graphs
    if gname == "3D-Grid"
        n = 125000
        x = Int64(round(n^(1/3)))
        G = LightGraphs.grid([x,x,x],periodic=true)
    elseif gname == "K-regular"
        n = 10000
        k = 20
        G = LightGraphs.random_regular_graph(n,k)
    elseif gname == "Barabasi-Albert"
        n = 10000
        k = 10
        G = LightGraphs.barabasi_albert(n,k)
    elseif gname == "Amazon"
        G = loadsnap(:amazon0302)
        G = LightGraphs.SimpleGraph(LightGraphs.SimpleDiGraph(Graphs.adjacency_matrix(G)))
    elseif gname == "Collab-CM"
        G = loadsnap(:ca_condmat)
        G = LightGraphs.SimpleGraph(LightGraphs.SimpleDiGraph(Graphs.adjacency_matrix(G)))
    elseif gname == "Citation-HEP"
        G = loadsnap(:arxiv_cit_hep_ph)
        G = LightGraphs.SimpleGraph(LightGraphs.SimpleDiGraph(Graphs.adjacency_matrix(G)))
    elseif gname == "Google"
        G = loadsnap(:web_google)
        G = LightGraphs.SimpleGraph(LightGraphs.SimpleDiGraph(Graphs.adjacency_matrix(G)))
    elseif gname == "US-Patent"
        G = loadsnap(:patent_cit_us)
        G = LightGraphs.SimpleGraph(LightGraphs.SimpleDiGraph(Graphs.adjacency_matrix(G)))
    end

    if(!LightGraphs.is_connected(G))
        cc=LightGraphs.connected_components(G)
        cc_length = length.(cc)
        cc_idx = argmax(cc_length)
        W = LightGraphs.adjacency_matrix(G)
        G = LightGraphs.SimpleGraph(W[cc[cc_idx],cc[cc_idx]])
    end
    n = LightGraphs.nv(G)
    d = LightGraphs.degree(G)
    L = LightGraphs.laplacian_matrix(G)

    sq_over_n =  10 .^ range( log10(0.01), log10(0.6), length = 8 )
    Q = zeros(length(sq_over_n))
    for (ridx,r) in enumerate(sq_over_n)
        sq_loss(q) = (sum( q ./ (q .+ d) ) - n*r)^2
        res = optimize(sq_loss,0,100000000)
        Q[ridx] = Optim.minimizer(res)[1]
    end
    @assert all( Q .> 0.0 )
    display("Q vals Set")

    # Experiment params.
    sq_over_n = zeros(length(Q))
    NREP = 100
    EXPREP = 51

    ## EFF TIME
    eff_time_direct = zeros(length(Q),EXPREP)
    eff_time_amg = zeros(length(Q),EXPREP)
    eff_time_pcg = zeros(length(Q),EXPREP)
    eff_time_cg = zeros(length(Q),EXPREP)

    eff_time_rsf = zeros(length(Q),EXPREP)
    eff_time_rsf_cv = zeros(length(Q),EXPREP)
    eff_time_rsf_cv2 = zeros(length(Q),EXPREP)
    eff_time_rsf_st = zeros(length(Q),EXPREP)
    eff_time_rsf_st_appr = zeros(length(Q),EXPREP)

    ## Var estimates
    var_direct = zeros(length(Q),EXPREP)
    var_amg = zeros(length(Q),EXPREP)
    var_pcg = zeros(length(Q),EXPREP)
    var_cg = zeros(length(Q),EXPREP)

    var_rsf = zeros(length(Q),EXPREP)
    var_rsf_cv = zeros(length(Q),EXPREP)
    var_rsf_cv2 = zeros(length(Q),EXPREP)
    var_rsf_st = zeros(length(Q),EXPREP)
    var_rsf_st_appr = zeros(length(Q),EXPREP)

    NREP_large = NREP^2
    exact=zeros(length(Q))
    for qidx in 1:length(Q)
        q = Q[qidx]

        display("gname:$gname,nv:$n,ne:$(LightGraphs.ne(G)),q:$q,qidx:$qidx")
        sum_exact = 0
        if(n > 10000)
            display("Exact Solution estimated via Hutchinson est with CG")
            for i in ProgressBar(1 : NREP)
                R = Float64.(2 .* bitrand(n,NREP) .- 1)
                X,_ = blockCG(L+q*I,R,maxIter=70)
                @turbo for m =1 : NREP
                    for k = 1 : n
                        sum_exact += q*R[k,m]*X[k,m]
                    end
                end
            end
            exact[qidx]= sum_exact/(NREP^2)
        else
            display("Exact Solution via direct computation")
            exact[qidx]= q*tr(inv(Matrix(L+q*I)))
        end

        for exprep in ProgressBar(1: EXPREP)
            res_direct = zeros(NREP)
            res_amg = zeros(NREP)
            res_pcg = zeros(NREP)
            res_cg = zeros(NREP)

            res_rsf = zeros(NREP)
            res_rsf_cv = zeros(NREP)
            res_rsf_cv2 = zeros(NREP)

            sq_over_n[qidx] = exact[qidx]/ n
            display("gname:$gname,q:$q,sq_over_n[qidx]:$(sq_over_n[qidx]),qidx:$qidx,exprep:$exprep")
            R = Float64.(2 .* bitrand(n,NREP) .- 1)

            ## Backslash
            display("Backslash,gname:$gname,q:$q,sq_over_n[qidx]:$(sq_over_n[qidx]),qidx:$qidx,exprep:$exprep")
            res_direct = zeros(NREP)
            eff_time_direct[qidx,exprep] = @elapsed begin
                X = (L+q*I)\R
                @turbo for m =1 : NREP
                    for k = 1 : n
                        res_direct[m] += q*R[k,m]*X[k,m]
                    end
                end
            end
            var_direct[qidx,exprep] = (mean(res_direct) .- exact[qidx]).^2
            ## AMG
            display("AMG,gname:$gname,q:$q,sq_over_n[qidx]:$(sq_over_n[qidx]),qidx:$qidx,exprep:$exprep")
            res_amg = zeros(NREP)
            eff_time_amg[qidx,exprep] = @elapsed begin
                ml = ruge_stuben(L+q*I) # Construct a Ruge-Stuben solver
                for i = 1 : NREP
                    temp = AlgebraicMultigrid.solve(ml, R[:,i],maxiter =20)
                    res_amg[i] += q*dot(R[:,i],temp)
                end
            end
            var_amg[qidx,exprep] = (mean(res_amg) .- exact[qidx]).^2

            ## PCG
            display("PCG,gname:$gname,q:$q,sq_over_n[qidx]:$(sq_over_n[qidx]),qidx:$qidx,exprep:$exprep")
            res_pcg = zeros(NREP)
            eff_time_pcg[qidx,exprep] = @elapsed begin
                ml = ruge_stuben(L+q*I) # Construct a Ruge-Stuben solver
                p = aspreconditioner(ml)
                function Mfun(X)
                    Z = similar(X)
                    for i=1: size(X,2)
                        Z[:,i] = p \ X[:,i]
                    end
                    return Z
                end
                X,_ = blockCG(L+q*I,R,M=Mfun)
                @turbo for m =1 : NREP
                    for k = 1 : n
                        res_pcg[m] += q*R[k,m]*X[k,m]
                    end
                end
            end
            var_pcg[qidx,exprep] = (mean(res_pcg) .- exact[qidx]).^2
            ## CG
            display("CG,gname:$gname,q:$q,sq_over_n[qidx]:$(sq_over_n[qidx]),qidx:$qidx,exprep:$exprep")

            res_cg = zeros(NREP)
            eff_time_cg[qidx,exprep] = @elapsed begin
                X,_ = blockCG(L+q*I,R,maxIter=70)
                @turbo for m =1 : NREP
                    for k = 1 : n
                        res_cg[m] += q*R[k,m]*X[k,m]
                    end
                end
            end
            var_cg[qidx,exprep] = (mean(res_cg) .- exact[qidx]).^2
            ## RSF Estimator
            display("RSF,gname:$gname,q:$q,sq_over_n[qidx]:$(sq_over_n[qidx]),qidx:$qidx,exprep:$exprep")

            res_rsf = zeros(NREP)
            eff_time_rsf[qidx,exprep] = @elapsed begin
                for nr = 1 : NREP
                    res_rsf[nr] = random_forest(G,q).nroots
                end
            end
            var_rsf[qidx,exprep] = (mean(res_rsf) .- exact[qidx]).^2
            ## RSF with Control Variate
            display("RSF with CV,gname:$gname,q:$q,sq_over_n[qidx]:$(sq_over_n[qidx]),qidx:$qidx,exprep:$exprep")

            res_rsf_cv = zeros(NREP)
            eff_time_rsf_cv[qidx,exprep] = @elapsed begin
                α = q ./ (q + mean(d))
                for nr = 1 : NREP
                    rf = random_forest(G,q)
                    rf,boundary_track = random_forest_boundary_track(G,q)
                    part=Int.(denserank(rf.root))
                    sizep = counts(part)
                    diag_S = 1 ./ sizep[part]

                    res_rsf_cv[nr] = ( rf.nroots- (α/q)*( diag_S'*boundary_track ) + α*(n - rf.nroots)  )
                end
            end
            var_rsf_cv[qidx,exprep] = (mean(res_rsf_cv) .- exact[qidx]).^2
            ## RSF with Control Variate-2
            display("RSF with CV2,gname:$gname,q:$q,sq_over_n[qidx]:$(sq_over_n[qidx]),qidx:$qidx,exprep:$exprep")

            res_rsf_cv2 = zeros(NREP)
            eff_time_rsf_cv2[qidx,exprep] = @elapsed begin
                α = q ./ (q + mean(d))
                for nr = 1 : NREP
                    rf,cont_var = random_forest_root_boundary_track(G,q)
                    res_rsf_cv2[nr] = ( rf.nroots- (α/q)*( cont_var) + α*(n - rf.nroots)  )
                end
            end
            var_rsf_cv2[qidx,exprep] = (mean(res_rsf_cv2) .- exact[qidx]).^2
            display("RSF with Stratification,gname:$gname,q:$q,sq_over_n[qidx]:$(sq_over_n[qidx]),qidx:$qidx,exprep:$exprep")

            ## RSF with First Layer Root Stratification - NORMAL APPROXIMATION
            eff_time_rsf_st_appr[qidx,exprep] = @elapsed begin
                prob_vec = q ./ (q .+ d) ## Edge probabilities\
                μ = sum(prob_vec)
                σ = sum(prob_vec .* (1 .- prob_vec))^(1/2)
                b = Normal(μ,σ)
                strat_f =  quantile.(b,collect(0.2:0.2:0.8))
                n_strats = length(strat_f)+1
                strats_p = repeat([0.2],n_strats)

                strat_bins = separator2bins(strat_f,n) ## Strat bins are calculated
                # Stratification
                size_st = round.(NREP*strats_p)

                tr_per_st = zeros(n_strats)
                @inbounds for binidx = 1 : n_strats
                    @inbounds for i = 1 : size_st[binidx]
                        fl = cond_bernouilli_sampler(prob_vec,strat_bins[binidx,:])
                        rf = random_forest_cond_first_layer(G,q,BitSet(fl);plot_flag=false)
                        tr_per_st[binidx] += rf.nroots
                    end
                end
                size_st[size_st .== 0] .= eps() # To avoid division by zero
                tr_per_st = (tr_per_st ./ size_st)'
                tr_st = tr_per_st * strats_p
            end
            var_rsf_st_appr[qidx,exprep] = (tr_st - exact[qidx] )^2
        end
        outputfolder = string("./outputs/",gname)
        JLD2.@save string(outputfolder,"/workspace.jld2") G sq_over_n exact Q NREP EXPREP eff_time_direct eff_time_amg eff_time_pcg eff_time_cg eff_time_rsf eff_time_rsf_cv eff_time_rsf_cv2 eff_time_rsf_st eff_time_rsf_st_appr var_direct var_amg var_pcg var_cg var_rsf var_rsf_cv var_rsf_cv2 var_rsf_st var_rsf_st_appr
    end

    tol = 0.002

    std_direct = sqrt.(mean(var_direct[:,2:end],dims=2))
    k_direct = ((std_direct ./ ( tol .* (exact) )).^2)

    std_amg = sqrt.(mean(var_amg[:,2:end],dims=2))
    k_amg = ((std_amg ./ ( tol .* (exact) )).^2)

    std_pcg = sqrt.(mean(var_pcg[:,2:end],dims=2))
    k_pcg = ((std_pcg ./ ( tol .* (exact) )).^2)

    std_cg = sqrt.(mean(var_cg[:,2:end],dims=2))
    k_cg = ((std_cg ./ ( tol .* (exact) )).^2)

    std_rsf = sqrt.(mean(var_rsf[:,2:end],dims=2))
    k_rsf = ((std_rsf ./ ( tol .* (exact) )).^2)

    std_rsf_cv = sqrt.(mean(var_rsf_cv[:,2:end],dims=2))
    k_rsf_cv = ((std_rsf_cv ./ ( tol .* (exact) )).^2)

    std_rsf_cv2 = sqrt.(mean(var_rsf_cv2[:,2:end],dims=2))
    k_rsf_cv2 = ((std_rsf_cv2 ./ ( tol .* (exact) )).^2)

    std_rsf_st = sqrt.(mean(var_rsf_st[:,2:end],dims=2))
    k_rsf_st = ((std_rsf_st ./ ( tol .* (exact) )).^2)

    std_rsf_st_appr = sqrt.(mean(var_rsf_st_appr[:,2:end],dims=2))
    k_rsf_st_appr = ((std_rsf_st_appr ./ ( tol .* (exact) )).^2)


    ## Error Plots
    figure()
    plot(sq_over_n,std_direct, label="direct")
    plot(sq_over_n,std_amg, label="amg")
    plot(sq_over_n,std_pcg, label="pcg")
    plot(sq_over_n,std_cg, label="cg")

    plot(sq_over_n,std_rsf, label="rsf")
    plot(sq_over_n,std_rsf_cv, label="rsf-cv")
    plot(sq_over_n,std_rsf_cv2, label="rsf-cv2")
    plot(sq_over_n,std_rsf_st, label="rsf-st")
    plot(sq_over_n,std_rsf_st_appr, label="rsf-st-appr")
    xlabel("sq/n")
    ylabel("std")
    title(gname)
    legend()
    ## Time-ϵ Plot

    figure()
    eff_time_direct = mean(eff_time_direct[:,2:end],dims=2)
    plot(sq_over_n,(eff_time_direct) .* k_direct,label="direct",marker="x")
    text(sq_over_n[end]+0.01, eff_time_direct[end] * k_direct[end],"direct")

    eff_time_amg = mean(eff_time_amg[:,2:end],dims=2)
    plot(sq_over_n,(eff_time_amg) .* k_amg,label="amg",marker="d")
    text(sq_over_n[end]+0.01, eff_time_amg[end] * k_amg[end],"amg")

    eff_time_pcg = mean(eff_time_pcg[:,2:end],dims=2)
    plot(sq_over_n,(eff_time_pcg) .* k_pcg,label="pcg",marker="H")
    text(sq_over_n[end]+0.01, eff_time_pcg[end] * k_pcg[end],"pcg")

    eff_time_cg = mean(eff_time_cg[:,2:end],dims=2)
    plot(sq_over_n,(eff_time_cg) .* k_cg,label="cg",marker="o")
    text(sq_over_n[end]+0.01, eff_time_cg[end] * k_cg[end],"cg")

    eff_time_rsf = mean(eff_time_rsf[:,2:end],dims=2)
    plot(sq_over_n,eff_time_rsf .* k_rsf,label="rsf",marker="h")
    text(sq_over_n[end]+0.01, eff_time_rsf[end] * k_rsf[end],"rsf")

    eff_time_rsf_cv = mean(eff_time_rsf_cv[:,2:end],dims=2)
    plot(sq_over_n,eff_time_rsf_cv .* k_rsf_cv,label="rsf-cv",marker="*")
    text(sq_over_n[end]+0.01, eff_time_rsf_cv[end] * k_rsf_cv[end],"rsf-cv")

    eff_time_rsf_cv2 = mean(eff_time_rsf_cv2[:,2:end],dims=2)
    plot(sq_over_n,eff_time_rsf_cv2 .* k_rsf_cv2,label="rsf-cv2",marker="D")
    text(sq_over_n[end]+0.01, eff_time_rsf_cv2[end] * k_rsf_cv2[end],"rsf-cv2")

    eff_time_rsf_st = mean(eff_time_rsf_st[:,2:end],dims=2)
    plot(sq_over_n,eff_time_rsf_st .* k_rsf_st,label="rsf-st",marker="s")
    text(sq_over_n[end]+0.01,eff_time_rsf_st[end] * k_rsf_st[end],"rsf-st")

    eff_time_rsf_st_appr = mean(eff_time_rsf_st_appr[:,2:end],dims=2)
    plot(sq_over_n,eff_time_rsf_st_appr .* k_rsf_st_appr,label="rsf-st-appr",marker="s")
    text(sq_over_n[end]+0.01,eff_time_rsf_st_appr[end] * k_rsf_st_appr[end],"rsf-st-appr")

    title(gname)
    xlabel("s(q)/n")
    ylabel("Effective Runtime (sec.)")
    legend()
    yscale("log")
end
